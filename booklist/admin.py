from django.contrib import admin

from booklist.models import Book 
from booklist.models import Magazine
from booklist.models import BookReview, Author, Genre, Issue

# Register your models here.

admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Issue)
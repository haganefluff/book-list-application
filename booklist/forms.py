
from turtle import title
from django import forms
from .models import Book, Magazine
  
  

class  BookForm(forms.ModelForm):
  

    class Meta:
       
        model = Book
  
        
        fields = [
            'title',
            'author',
            'pages',
            'isbn',
            'year_published',
            'description',
            'image',
            'in_print'

        ]

class MagazineForm(forms.ModelForm):

    class Meta:
        model = Magazine

        fields = [ 

            'title',
            'release_cycle',
            'description',
            'cover_image',

        ]                 #[Magazine.object.all()]
        


from django.contrib import admin
from django.urls import path
from booklist.views import delete_magazine, magazines_list, create_magazine, show_magazine_details, show_genres_details
urlpatterns = [
    path("", magazines_list, name="magazines_list"),
    path('create/', create_magazine),
    path('<int:pk>/', show_magazine_details, name="show_magazine_details"),
    # path("<int:pk>/update", update_book, name="update")
    path('<int:pk>/delete/', delete_magazine, name="delete_magazine"),
    path('genre/', show_genres_details, name='show_genres_details')

]
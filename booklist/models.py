from django.db import models

# Create your models here.
from django.db import models

class  Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField("Author", related_name="books")
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.author.first())

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name



class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()


class Magazine(models.Model):
    title = models.CharField(max_length=100, unique=True)
    release_cycle = models.CharField(max_length=35)
    description = models.TextField(null=True)
    cover_image = models.URLField(null=True)
    genres = models.ManyToManyField("Genre", related_name='genre')

    def __str__(self):
        return self.title



class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name='issues', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = models.TextField()
    date_publish = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True, blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_image = models.URLField(null=True, blank=True)


    def __str__(self):
        return self.title



class Genre(models.Model):
    genre_name = models.CharField(max_length=200)
    def __str__(self):
        return self.genre_name
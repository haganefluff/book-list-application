from django.contrib import admin 
from django.urls import path
from booklist.views import create_view, show_book_details, show_books, update_book



urlpatterns = [
    path("", show_books),
    path('create/', create_view),
    path('<int:pk>/', show_book_details, name="show_book_details"),
    path("<int:pk>/update", update_book, name="update")

]
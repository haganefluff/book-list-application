from django.shortcuts import get_object_or_404, render, redirect
from booklist.forms import BookForm, MagazineForm
from booklist.models import Book, Genre, Magazine

def show_books(request):
    books = Book.objects.all()
    context = {
        "books" : books
        }
    print(books)
    return render(request, "books/list.html", context)


def create_view(request):
    
    context = {}

   
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("/books")

    context['form']= form
    return render(request, "books/create_view.html", context)


def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context = {
        "book": book
    }
    return render(request, 'books/show_book.html', context)


def update_book(request, pk):
    context ={}
    book = Book.objects.get(pk=pk)
    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect("show_book_details", pk=pk)

    context["form"] = form
    return render(request, "books/update.html", context)
    
def delete_book(request):
    pass


def magazines_list(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines" : magazines
        }

    print(magazines)   
    return render(request, "magazines/magazine_list.html", context)
    
    
def create_magazine(request):
    context = {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("magazines_list")

    context['form']= form
    return render(request, "magazines/create_view.html", context)


def show_magazine_details(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context = {
        "magazine": magazine
    }
    return render(request, 'magazines/show_magazine.html', context)


def delete_magazine(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    if request.method == "POST":
        magazine.delete()
        return redirect('magazines_list')
    context = {
        "magazine" : magazine
    }
    return render(request, 'magazines/delete_magazine.html', context)


def show_genres_details(request, pk):
    genres = Genre.objects.get(pk=pk)
    context = {
        'genre_var': genres
    }

    return render(request, 'magazines/genre.html', context)

# Create your views here.